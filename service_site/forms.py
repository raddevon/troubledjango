from django.contrib.localflavor.us.forms import USZipCodeField
from django.forms import ModelForm
from service_site.models import Site

class SiteForm(ModelForm):
    """
    ModelForm from Site for adding and editing sites serviced
    """
    class Meta:
        model = Site

        zip = USZipCodeField()
