from django.contrib.auth.models import User

class Solver(User):
    """
    Model for solvers (people who solve tickets). Subclass of default user model in contrib.user.
    """

    def __unicode__(self):
        return u'%s %s' % (self.first_name, self.last_name)

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)

    class Meta:
        permissions = (
        # Permissions on Solvers
            ('add_solvers', 'Add new solvers'),
            ('edit_solvers', 'Edit solvers'),
            ('edit_self', 'Edit own account'),
            ('delete_solvers', 'Delete solvers'),

        # Permissions on Tickets
            # Viewing permissions
            ('view_own_tickets', 'See tickets created by this user'),
            ('view_tickets', 'See tickets created by any user'),
            # Editing permissions
            ('edit_own_tickets', 'Edit tickets created by this user'),
            ('edit_tickets', 'Edit tickets created by any user'),
            # Creating permissions
            ('create_tickets', 'Create tickets'),
            # Assignment permissions
            ('assign_to_self', 'Assign tickets to this user'),
            ('assign', 'Assign tickets to any user'),
            ('reassign', 'Change ticket assignment'),
            # Closing permissions
            ('close_own_tickets', 'Close tickets assigned to this user'),
            ('close_tickets', 'Close tickets assigned to any user'),

        # Permissions on Sites
            ('view_sites', 'View sites'),
            ('edit_sites', 'Edit sites'),
            ('delete_sites', 'Delete sites')
        )