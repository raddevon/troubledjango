from django.conf.urls.defaults import patterns, include, url
from solver.views import addSolver

urlpatterns = patterns('',
    url(r'^add/$', addSolver),          # /solver/add
)
