from django.conf.urls.defaults import patterns, include, url
from service_site.views import addEditSite

urlpatterns = patterns('',
    url(r'^add/$', addEditSite),              # /site/add/
)
