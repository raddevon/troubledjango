from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from solver.forms import SolverForm

def addSolver(request):
    """
    Form for new solvers
    """
    if request.method == 'POST':
        form = SolverForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            # todo-devon Change the response redirect
            return HttpResponseRedirect("/books/")
    else:
        form = SolverForm()
    # Context includes form object, URL for form action (dynamically generated from argument passed as op),
    # and a title dynamically generated from operation combined with the object type in question.
    initialData = {'form': form, 'form_action': '/solver/add/', 'title': 'Add a solver'}

    csrfContext = RequestContext(request, initialData)              # adds CSRF token to form context
    return render_to_response('form.html', csrfContext)             # pass context with token to form