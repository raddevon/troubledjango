from django import forms
from tickets.models import Ticket, Update

# todo-devon User setting to hide form help text
class TicketForm(forms.ModelForm):
    """
    ModelForm from Ticket for adding and editing tickets
    """
    class Meta:
        model = Ticket

class TicketFilter(forms.ModelForm):
    """
    Filter options for ticket list
    """
    class Meta:
        model = Ticket
        # todo-devon State field giving errors. Not sure why.
#        fields = ('priority', 'sites', 'state')

class UpdateForm(forms.ModelForm):
    """
    ModelForm from Update for creating ticket status updates
    """

    def __init__(self, *args, **kwargs):
        # Pop ticket and instance from **kwargs or None if not present
        ticket = kwargs.pop('ticket', None)
        instance = kwargs.pop('instance', None)

        super(UpdateForm, self).__init__(instance=instance, *args, **kwargs)
        # If we're dealing with an instance of an update, look at that update's markings as well as the ticket state.
        if instance:
            # If the ticket is completed, removed the 'completed' field from the update form.
            if (ticket and ticket.state == 'completed') or instance.reopened:
                del self.fields['completed']
            # If the ticket is not completed, remove the 'reopen' field from the update form.
            elif (ticket and ticket.state != 'completed') or instance.completed:
                del self.fields['reopened']
        # If we're dealing with a new update, look only at the ticket state.
        else:
            # If the ticket is completed, removed the 'completed' field from the update form.
            if ticket and ticket.state == 'completed':
                del self.fields['completed']
            # If the ticket is not completed, remove the 'reopen' field from the update form.
            elif ticket and ticket.state != 'completed':
                del self.fields['reopened']

    class Meta:
        model = Update

class ReopenUpdateForm(UpdateForm):
    """
    A sub-class of the UpdateForm ModelForm that hides and checks the re-open checkbox.
    """
    reopened = forms.BooleanField(widget=forms.HiddenInput(), initial=True)
    completed = forms.BooleanField(widget=forms.HiddenInput(), initial=False)