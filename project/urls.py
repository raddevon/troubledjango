from django.conf.urls.defaults import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from project import local_settings

urlpatterns = patterns('',
    url(r'^solver/', include('solver.urls')),
    url(r'^ticket/', include('tickets.urls')),
    url(r'^site/', include('service_site.urls'))

)

# adds media path for debug mode
if local_settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve',
             {'document_root':     local_settings.MEDIA_ROOT}),
    )

# Sets media path for static files. Only fires in debug mode
urlpatterns += staticfiles_urlpatterns()