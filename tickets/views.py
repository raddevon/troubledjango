from django.shortcuts import render_to_response, redirect
from django.template.context import RequestContext
from tickets.forms import TicketForm, UpdateForm, ReopenUpdateForm
from tickets.models import Ticket, Update
from django.contrib import messages
from project.utils import get_datatables_records

def addTicket(request):
    """
    Validation and saving of new tickets
    """
    # If the form was submitted
    if request.method == 'POST':
        # Loads the form with the just submitted data so that it can be re-filled in case of an error.
        form = TicketForm(request.POST)

        if form.is_valid():
            form.save()
            # Confirmation for save of a new ticket to appear on next page load.
            messages.success(request,'Your new ticket has been saved.')
            # Redirect to a fresh add form if user clicks 'Submit + another'
            if "submit-plus" in request.POST:
                form = TicketForm()
                # Context includes form object, URL for form action (dynamically generated from argument passed as op),
                # and a title dynamically generated from operation combined with the object type in question.
                initialData = {'form': form, 'form_action': '/ticket/add/', 'title': 'Add a ticket'}
                csrfContext = RequestContext(request, initialData)              # adds CSRF token to form context
                return render_to_response('add_ticket.html', csrfContext)             # pass context with token to form
            # Redirect to ticket list
            return redirect('/ticket/')
        else:
            # Setup the contexts for the template
            initialData = {
                'form': form,
                'form_action': '/ticket/add/',
                'title': 'Add a ticket',
            }
            # include the CSRF token with the rest of the context
            csrfContext = RequestContext(request, initialData)
            # Error message to appear on the next page load.
            messages.error(request,"Your submission didn't check out. See the form for details.")
            # and build the ticket detail page with the context + CSRF token
            return render_to_response('add_ticket.html', csrfContext)
    # If no form was submitted and no existing key was in the URL, instantiate a new blank ticket form.
    else:
        form = TicketForm()
        # Context includes form object, URL for form action (dynamically generated from argument passed as op),
        # and a title dynamically generated from operation combined with the object type in question.
        initialData = {'form': form, 'form_action': '/ticket/add/', 'title': 'Add a ticket'}
        csrfContext = RequestContext(request, initialData)              # adds CSRF token to form context
        return render_to_response('add_ticket.html', csrfContext)             # pass context with token to form

def editTicket(request, encrypted_pk=None, url_cipher=None):
    """
    Processing for ticket editing and updating
    """
    # Decrypt the primary key captured from the URL
    pk = int(encrypted_pk) ^ url_cipher
    # Fetch the object matching the primary key passed in the URL
    ticket_instance = Ticket.objects.get_or_none(ticket_number=pk)

    # If the primary key is not found in the database
    if not ticket_instance:
        # Error message for unknown primary key
        messages.error(request,'The requested ticket was not found in the database.')
        # Redirect to ticket list
        return redirect('/ticket/')

    # Fetch updates for the current ticket. Compile them in a list.
    updates = []
    for update in Update.objects.filter(ticket=pk).order_by('-time'):
        updates.append(update)

#Begin processing submitted form
    # If the form was submitted, delete if the delete button was clicked. Validate and save the edits if the submit
    # button was clicked.
    if request.method == 'POST':

    # Begin update submission processing
        # Save and validation for submitted updates
        # Check for update form submit button in POST request
        if "submit-update" in request.POST:
            update_form = UpdateForm(request.POST, ticket=ticket_instance)
            reopen_update_form = ReopenUpdateForm()
            # Validate update
            if update_form.is_valid():
                # Save without commit. This way I can add which ticket it will attach to before committing.
                new_update = update_form.save(commit=False)
                # Add the ticket
                new_update.ticket = Ticket.objects.get_or_none(ticket_number=pk)
                # Save if valid
                new_update.save()
                # Confirmation for save of a new update to appear on next page load.
                messages.success(request,'Your new update has been saved.')
                # Change the ticket's state if the new update is marked as completed.
                if new_update.completed:
                    if ticket_instance:
                        # Set state to 'completed'
                        ticket_instance.state = 'completed'
                        # Save ticket
                        ticket_instance.save()
                        messages.info(request, 'This ticket was marked as completed.')
                # If a new update is submitted not marked as completed and the ticket state is not pending, make it so.
                elif ticket_instance.state != 'pending':
                    ticket_instance.state = 'pending'
                    ticket_instance.save()
                    messages.info(request, 'This ticket was marked as pending.')

                # Redirect to current ticket
                return redirect('/ticket/edit/' + encrypted_pk + '/')

            # If update form submitted is not valid
            else:
                # fetch the appropriate object using the decrypted key,
                ticket  = Ticket.objects.get_or_none(pk=pk)
                # instantiate the form with the object,
                form = TicketForm(instance=ticket)
                # setup the contexts for the template,
                initialData = {
                    'form': form,
                    'update_form': update_form,
                    'reopen_update_form': reopen_update_form,
                    'form_action': '/ticket/edit/' + encrypted_pk + '/',
                    'title': 'Edit this ticket',
                    'ticket': ticket,
                    'updates': updates,
                    'update_form_visible': True
                }
                # include the CSRF token with the rest of the context
                csrfContext = RequestContext(request, initialData)
                # Error message to appear on the next page load.
                messages.error(request,"Your submission didn't check out. See the form for details.")
                # and build the ticket detail page with the context + CSRF token
                return render_to_response('ticket_detail.html', csrfContext)
    # End update submission processing

    # Begin ticket re-opening processing
        # Save and validation for submitted updates
        # Check for update form submit button in POST request
        if "submit-reopen-update" in request.POST:
            reopen_update_form = ReopenUpdateForm(request.POST, ticket=ticket_instance)
            update_form = UpdateForm(ticket=ticket_instance)
            # Validate update
            if reopen_update_form.is_valid():
                # Save without commit. This way I can add which ticket it will attach to before committing.
                new_update = reopen_update_form.save(commit=False)
                # Add the ticket
                new_update.ticket = Ticket.objects.get_or_none(ticket_number=pk)
                # Save if valid
                new_update.save()
                # Confirmation for save of a new update to appear on next page load.
                messages.success(request,'Your new update has been saved.')
                # Change the ticket's state if the new update is marked as completed.
                ticket_instance.state = 'pending'
                ticket_instance.save()
                messages.info(request, 'This ticket was marked as pending.')

                # Redirect to current ticket
                return redirect('/ticket/edit/' + encrypted_pk + '/')

            # If update form submitted is not valid
            else:
                # fetch the appropriate object using the decrypted key,
                ticket  = Ticket.objects.get_or_none(pk=pk)
                # instantiate the form with the object,
                form = TicketForm(instance=ticket)
                # setup the contexts for the template,
                initialData = {
                    'form': form,
                    'update_form': update_form,
                    'reopen_update_form': reopen_update_form,
                    'form_action': '/ticket/edit/' + encrypted_pk + '/',
                    'title': 'Edit this ticket',
                    'ticket': ticket,
                    'updates': updates,
                    'reopen_update_form_visible': True
                }
                # include the CSRF token with the rest of the context
                csrfContext = RequestContext(request, initialData)
                # and build the ticket detail page with the context + CSRF token
                return render_to_response('ticket_detail.html', csrfContext)
    # End ticket re-opening processing



    # Begin validation and saving of ticket edits
        # Insures proper saving if form is an object instance
        form = TicketForm(request.POST, instance=ticket_instance)

        # Handles ticket deletion
        if request.POST.get('delete'):
            ticket_instance.delete()
            # Confirmation message for deletion will be displayed on the next page load.
            messages.success(request,'The ticket was successfully deleted.')
            # Redirect to ticket list
            return redirect('/ticket/')

        # Handles saving of edits to existing objects
        if form.is_valid():
            form.save()
            # Confirmation for edits to appear on the next page load.
            messages.success(request,'Your ticket has been edited.')
            # Redirect to ticket list
            return redirect('/ticket/')
        else:
            # instantiate update form
            update_form = UpdateForm(ticket=ticket_instance)
            reopen_update_form = ReopenUpdateForm()
            # setup the contexts for the template,
            initialData = {
                'form': form,
                'update_form': update_form,
                'reopen_update_form': reopen_update_form,
                'form_action': '/ticket/edit/' + encrypted_pk + '/',
                'title': 'Edit this ticket',
                'ticket': ticket_instance,
                'updates': updates,
                'ticket_form_visible': True
            }
            # include the CSRF token with the rest of the context
            csrfContext = RequestContext(request, initialData)
            # Error message to appear on the next page load.
            messages.error(request,"Your submission didn't check out. See the form for details.")
            # and build the ticket detail page with the context + CSRF token
            return render_to_response('ticket_detail.html', csrfContext)
    # End validation and saving of ticket edits

    # instantiate the form with the object,
    form = TicketForm(instance=ticket_instance)
    # create a form for adding updates,
    update_form = UpdateForm(ticket=ticket_instance)
    reopen_update_form = ReopenUpdateForm()
    # setup the contexts for the template,
    initialData = {
        'form': form,
        'update_form': update_form,
        'reopen_update_form': reopen_update_form,
        'form_action': '/ticket/edit/' + encrypted_pk + '/',
        'title': 'Edit this ticket',
        'edit_form': True,
        'ticket': ticket_instance,
        'updates': updates
    }
    # include the CSRF token with the rest of the context
    csrfContext = RequestContext(request, initialData)
    # and build the ticket detail page with the context + CSRF token
    return render_to_response('ticket_detail.html', csrfContext)

def get_tickets_list(request, queryset=Ticket.objects.all()):
    """
    AJAX calls for DataTable ticket list.
    """

    #columnIndexNameMap is required for correct sorting behavior
    columnIndexNameMap = { 0: 'link', 1 : 'created', 2: 'priority', 3: 'client', 4: 'client_email', 5: 'sites', 6: 'problem',7: 'updates', 8: 'state' }
    #path to template used to generate json (optional)
    jsonTemplatePath = 'json_tickets.txt'

    #call to generic function from utils
    return get_datatables_records(request, queryset, columnIndexNameMap, jsonTemplatePath)