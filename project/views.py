from django.shortcuts import redirect

def redirect_wrapper(trash, url=None, perm=False):
    """
    Wraps the redirect function to discard trash parameters passed by bad URLs. Used for catch-all redirects.
    url=[None] Destination URL
    perm=True/[False] Permanent redirect?
    """
    if url:
        return redirect(to=url, permanent=perm)