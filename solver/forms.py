from django.contrib.auth.forms import UserCreationForm
from solver.models import Solver

class SolverForm(UserCreationForm):

    class Meta:
        # Displays fields not displayed by default in the UserCreationForm
        fields = ("username", "first_name", "last_name", "email", "username",)
        model = Solver

    def __init__(self, *args, **kwargs):
        # Allows modification of field attributes
        super(SolverForm, self).__init__(*args, **kwargs)

        # todo-devon It may be a good idea to make these user-selectable in the future.
        # Require email address, first name, and last name
        self.fields['email'].required = True
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        # Changes help text for username field to display more information
        self.fields['username'].help_text = """
        Letters, digits and @/./+/-/_ only.
        Your full name, not this username, will be used for ticket assignment.
        """