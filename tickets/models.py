from django.db import models
from service_site.models import Site
from solver.models import Solver
from project.url_cipher import URL_CIPHER

class GetOrNoneManager(models.Manager):
    """
    Adds get_or_none method to objects.
    """
    def get_or_none(self, **kwargs):
        try:
            return self.get(**kwargs)
        except self.model.DoesNotExist:
            return None

class Ticket(models.Model):
    """Model for trouble tickets"""
    # todo-devon Look for a better way to do created and modified fields. Check django-model-utils
    ticket_number = models.AutoField(primary_key=True, editable=False)
    # Date and time the ticket was created
    created = models.DateTimeField(auto_now_add=True, editable=False)
    client = models.CharField(max_length=50, verbose_name='client name', help_text='Which user is having this problem?')
    client_email = models.EmailField(blank=True, verbose_name='client e-mail', help_text='Give us an email address to contact them.')
    sites = models.ManyToManyField(Site)
    # Overrides dumb select box help text
    sites.help_text = 'Select the sites where the problem is taking place.'
    # problem = a short description. This will be used when space is at a premium.
    problem = models.CharField(max_length=100, help_text='Enter a quick description of the problem. A short sentence will do.')
    # problem_description = full description. This will be used on the individual ticket's page.
    problem_description = models.TextField(help_text='Now, we want all the details.')
    # The ticket has three possible states. The state is automatically set to 'new' upon creation. It will be modifiable
    # when the ticket is updated
    state = models.CharField(max_length=10, choices=(
        ('new', 'New'),
        ('pending', 'Pending'),
        ('completed', 'Completed'),
    ), blank=True, default='new', editable=False)
    # Ticket priority setting
    priority = models.CharField(max_length=10, choices=(
        ('1', 'Low'),
        ('2', 'Normal'),
        ('3', 'High'),
        ('4', 'Critical'),
        ), default='2', help_text='How important is it?')
    # Ticket is assigned to a solver
    assigned = models.ManyToManyField(Solver)
    # Overrides dumb select box help text
    assigned.help_text = 'Who should fix it?'

    objects = GetOrNoneManager()

    def getURL(self):
        encrypted_pk = int(self.ticket_number ^ URL_CIPHER)
        return str(encrypted_pk)

    class Meta:
        get_latest_by = 'created'
        # Ordered by date created then by client name
        ordering = ['-created', 'client']

    def __unicode__(self):
        # Unicode repr '[Problem] for [client]'
        return u'%s for %s' % (self.problem, self.client)


class Update(models.Model):
    """
    Model for updates to trouble tickets. Updates can contain information about the actions perform and can update data
    in the original ticket. Information added to the ticket rather than changed from the original is contained in this
    model.
    """
    # Time of the update
    time = models.DateTimeField(auto_now_add=True, editable=False)
    # Short update description. Used for small spaces
    title = models.CharField(max_length=100, help_text='Give the update a short title.')
    # Verbose update description with full detail shown on individual ticket page
    description = models.TextField(help_text='Tell us all about it here.')
    # Relationship of update to a ticket
    ticket = models.ForeignKey(Ticket, editable=False)
    # Does this update complete the ticket?
    completed = models.BooleanField(default=False, verbose_name='completed?', help_text='Does this update close the ticket?')
    # Does this update re-open the ticket?
    reopened = models.BooleanField(default=False, verbose_name='re-open?', help_text='Check to re-open the ticket with this update.')

    objects = GetOrNoneManager()

    def getURL(self):
        encrypted_pk = int(self.id ^ URL_CIPHER)
        return str(encrypted_pk)

    class Meta:
        get_latest_by = 'time'
        # Ordered by 'time' descending (latest first)
        ordering = ['-time']
        # Ordered with respect to associated ticket
        order_with_respect_to = 'ticket'

    def __unicode__(self):
        # Unicode repr '[Ticket] update: [title]'
        return u'%s update: %s' % (self.ticket, self.title)