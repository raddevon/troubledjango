from django.shortcuts import render_to_response
from django.template.context import RequestContext
from service_site.forms import SiteForm

# todo-devon Split addSite and editSite
def addEditSite(request, op='add'):
    """
    Form and processing for new sites
    """
    if request.method == 'POST':
        form = SiteForm(request.POST)
        if form.is_valid():
            form.save()
            # Context includes form object, URL for form action (dynamically generated from argument passed as op),
            # and a title dynamically generated from operation combined with the object type in question.
            initialData = {'form': form, 'form_action': '/site/' + op, 'title': op.capitalize() + ' a site'}
            csrfContext = RequestContext(request, initialData)      # adds CSRF token to form context
            # todo-devon Adding a site should return a view of a list of sites.
            return render_to_response('form.html', csrfContext)     # pass context with token to form
    else:
        form = SiteForm()

    # Context includes form object, URL for form action (dynamically generated from argument passed as op),
    # and a title dynamically generated from operation combined with the object type in question.
    initialData = {'form': form, 'form_action': '/site/' + op + '/', 'title': op.capitalize() + ' a site'}
    csrfContext = RequestContext(request, initialData)              # adds CSRF token to form context
    return render_to_response('form.html', csrfContext)             # pass context with token to form