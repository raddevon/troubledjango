function checkAllNone(select_elem, name) {
    var select_none = '.select-none[data-name=' + name + ']'
    var select_all = '.select-all[data-name=' + name + ']'

    if (!($(select_elem).val())) {
        $(select_none).addClass('active');
        $(select_all).removeClass('active');
    } else if ($(select_elem).val().length === $(select_elem + ' option').length) {
        $(select_all).addClass('active');
        $(select_none).removeClass('active');
    } else {
        $(select_none + ',' + select_all).removeClass('active');
    }
}

// Maps a button group and to a hidden select widget
$(function () {
    $('.btn-select-vertical,.btn-select').on('click', 'label[data-name]', function() {
        var select = $(this).data('name').split(',');
        var name = select[0];
        var value = select[1];
        var select_elem = 'select#' + name;

        // Test for the active class on the button. If present, select the option in the select widget.
        if ($(this).hasClass('active')) {
            $(select_elem + ' option[value="' + value + '"]').removeAttr('selected');
        // If button is not active, deselect the option in the select widget.
        } else {
            $(select_elem + ' option[value="' + value + '"]').attr('selected', 'selected');
        }

        checkAllNone(select_elem, name);

    });
});

// Selects all options and activates all buttons if "All" button is clicked
$(function () {
    $('.select-all').click(function() {
        var name = $(this).parent().data('name');
        var select_elem = 'select#' + name;
        var buttons = '.btn-select-vertical#' + name + ' label,btn-select#' + name + ' label';

        $(buttons).each(function() {
            $(this).addClass('active');
        });

        $(select_elem + ' option').attr("selected","selected");
    });
});

// Deselects all options and deactivates all buttons when "None" button is clicked
$(function () {
    $('.select-none').click(function() {
        var name = $(this).parent().data('name');
        var select_elem = 'select#' + name;
        var buttons = '.btn-select-vertical#' + name + ' label,btn-select#' + name + ' label';

        $(buttons).each(function() {
            $(this).removeClass('active');
        });

        $(select_elem + ' option').removeAttr('selected');
    });
});

// Maps selections in a select multiple widget to the button group
$(document).ready(function() {
    $('.button-toggles').change(function() {
        var name = $(this).attr('id');
        var selected = $(this).val();
        var select_elem = 'select#' + name;
        var buttons = '.btn-select-vertical#' + name + ' label,btn-select#' + name + ' label';

        // Deactivate all buttons in current group
        $(buttons).each(function() {
            $(this).removeClass('active');
        });

        // Loop through values in select activating the correct buttons
        if (selected) {
            for (i=-1; i<selected.length; i++) {
                var button = $('#btn-' + name + '-' + selected[i]);
                if (!(button.hasClass('active'))) {
                    button.addClass('active');
                }
            }
        }

        checkAllNone(select_elem, name);
    });
});

// Triggers a change event on the select widget when the document loads to set buttons to their initial states
$(document).ready(function() {
    $('.button-toggles').change();
});

// Insures that buttons will behave as buttons.
$('.btn').button();