from django.db import models
from django.contrib.localflavor.us import models as usmodels
from solver.models import Solver
from project.url_cipher import URL_CIPHER

class GetOrNoneManager(models.Manager):
    """
    Adds get_or_none method to objects.
    """
    def get_or_none(self, **kwargs):
        try:
            return self.get(**kwargs)
        except self.model.DoesNotExist:
            return None

class Site(models.Model):
    """Model for sites serviced"""
    # Name of site
    name = models.CharField(max_length=50, unique=True)
    # Phone number (optional)
    phone = usmodels.PhoneNumberField(blank=True)
    # Address of site (optional)
    address_1 = models.CharField(max_length=100, blank=True, verbose_name='address')
    # Optional second address line
    address_2 = models.CharField(max_length=100, blank=True)
    city = models.CharField(max_length=50, blank=True)
    state = usmodels.USStateField(blank=True)
    zip = models.CharField(max_length=15, blank=True )
    # Relationship to one or many solvers for default ticket assignment. (optional)
    primary_solvers = models.ManyToManyField(Solver, blank=True)
    primary_solvers.help_text = "Select one or more solvers who should be assigned these tickets by default."

    objects = GetOrNoneManager()

    def getURL(self):
        encrypted_pk = int(self.id ^ URL_CIPHER)
        return str(encrypted_pk)

    class Meta:
        # Sorted alphabetically
        ordering = ['name']

    def __unicode__(self):
        return unicode(self.name)

    def __str__(self):
        return self.name