from django.conf.urls import patterns, url
from django.views.generic.list import ListView
from project.url_cipher import URL_CIPHER
from tickets.views import addTicket, editTicket, get_tickets_list
from tickets.models import Ticket
from project.views import redirect_wrapper

urlpatterns = patterns('',
    # /ticket/add/
    url(r'^add/*$', addTicket),

    # /ticket/edit/[ticket number]/
    url(r'^edit/(?P<encrypted_pk>\d+)/*$', editTicket, {'url_cipher':URL_CIPHER}),

    # /ticket/
    url(r'^$', ListView.as_view(
            model=Ticket,
            context_object_name='ticket_list',
            )
        ),

    # For ajax loading of ticket tables
    url(r'^ajax/get-tickets-list/$', get_tickets_list, name = 'get_tickets_list'),

    # Matches any other unmatched URL
    url(r'^.*', redirect_wrapper, {'url':'/ticket/', 'perm': 'True'}),
)
